import tensorflow as tf
from headliner.model.bert_summarizer import BertSummarizer
from fastapi import FastAPI
import uvicorn

from pydantic import BaseModel


model = BertSummarizer.load('data/bert_summarizer')


class Itemask(BaseModel):
    ask: str


def get_reply(model, msg):
    prd = model.predict(msg)
    prd = prd.strip('[SEP]').strip()
    return prd

app = FastAPI()

@app.get("/")
def read_root():
    return {"Hello": "World"}

@app.post("/translator/translate")
def predict(item: Itemask):
    ask = item.ask
    ans =  get_reply(model, ask)
    return  {'Answer':ans} 

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=5000, log_level="info", reload=False)
