import os

os.system('pip install --upgrade grpcio')
os.system('pip install -U headliner')
os.system('pip install -U tensorflow_datasets')


# Create the dataset
import io

def create_dataset(path, num_examples):
    lines = io.open(path, encoding='UTF-8').read().strip().split('\n')
    word_pairs = [[w for w in l.split('\t')[:2]]  for l in lines[:num_examples]]
    return zip(*word_pairs)

ask, ans = create_dataset('/kaggle/input/headlinerdata/askans.txt',1330724)
# eng, ger = create_dataset('/kaggle/input/headlinerdata/askans.txt',5000)
data = list(zip(ask, ans))

from sklearn.model_selection import train_test_split

train, test = train_test_split(data, test_size=5000)

# Define custom preprocessing
from headliner.preprocessing.bert_preprocessor import BertPreprocessor
# from spacy.lang.en import English
from spacy.lang.fi import Finnish

# preprocessor = BertPreprocessor(nlp=English())
preprocessor = BertPreprocessor(nlp=Finnish())

train_prep = [preprocessor(t) for t in train]

# Fit custom tokenizers for input and target
from tensorflow_datasets.core.features.text import SubwordTextEncoder
from transformers import TFBertModel, BertTokenizer
from headliner.preprocessing.bert_vectorizer import BertVectorizer

targets_prep = [t[1] for t in train_prep]
# tokenizer_input = BertTokenizer.from_pretrained('/kaggle/input/bertfinnishuncasedpytorchdata')
tokenizer_input = BertTokenizer.from_pretrained('TurkuNLP/bert-base-finnish-uncased-v1')
tokenizer_target = SubwordTextEncoder.build_from_corpus(
    targets_prep, target_vocab_size=2**13, 
    reserved_tokens=[preprocessor.start_token, preprocessor.end_token])

vectorizer = BertVectorizer(tokenizer_input, tokenizer_target)
'vocab size input {}, target {}'.format(
    vectorizer.encoding_dim, vectorizer.decoding_dim)

import tensorflow as tf
from headliner.model.bert_summarizer import BertSummarizer
from headliner.trainer import Trainer

summarizer = BertSummarizer(num_heads=8,
                            feed_forward_dim=1024,
                            num_layers_encoder=0,
                            num_layers_decoder=4,
                            bert_embedding_encoder='TurkuNLP/bert-base-finnish-uncased-v1',
                            embedding_size_encoder=768,
                            embedding_size_decoder=768,
                            dropout_rate=0,
                            max_prediction_len=50)   
# Adjust learning rates of encoder and decoder optimizer schedules
# You may want to try different learning rates and observe the loss
summarizer.optimizer_decoder = BertSummarizer.new_optimizer_decoder(
    learning_rate_start=1e-3  #2e-2
)
summarizer.optimizer_encoder = BertSummarizer.new_optimizer_encoder(
    learning_rate_start=1e-3 #5e-4
)
summarizer.init_model(preprocessor, vectorizer)
trainer = Trainer(steps_per_epoch=5000,
                  batch_size=32, #16 32?
                  model_save_path='bert_summarizer',
                  tensorboard_dir='/tmp/bert_tensorboard',
                  steps_to_log=100) #20
trainer.train(summarizer, train, num_epochs=16, val_data=test)

best_summarizer = BertSummarizer.load('bert_summarizer')

f = open("0seq2seqres.txt", "w")

filepath = '/kaggle/input/headlinerdata/test.txt'
with open(filepath) as fp:
   line = fp.readline()
   while line:
       prd = best_summarizer.predict(line.strip())
       outline = line.strip() + ' --> ' +prd +'\n'
       f.write(outline.strip('[SEP]')) 
       line = fp.readline()
       
f.close()


