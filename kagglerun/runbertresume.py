import os

os.system('pip install --upgrade grpcio')
os.system('pip install -U headliner')
os.system('pip install -U tensorflow_datasets')


# Create the dataset
import io

def create_dataset(path, num_examples):
    lines = io.open(path, encoding='UTF-8').read().strip().split('\n')
    word_pairs = [[w for w in l.split('\t')[:2]]  for l in lines[:num_examples]]
    return zip(*word_pairs)

ask, ans = create_dataset('/kaggle/input/headlinerdata/askans.txt',1330724)
# ask, ans = create_dataset('/kaggle/input/headlinerdata/askans.txt',5000)
data = list(zip(ask, ans))

from sklearn.model_selection import train_test_split

train, test = train_test_split(data, test_size=5000)



import tensorflow as tf
from headliner.model.bert_summarizer import BertSummarizer
from headliner.trainer import Trainer

summarizer = BertSummarizer.load('/kaggle/input/headlinerdata/bert_summarizer')
trainer = Trainer(batch_size=32,model_save_path='bert_summarizer')
trainer.train(summarizer, train, num_epochs=185, val_data=test) #185

# Load best model and do some prediction
best_summarizer = BertSummarizer.load('bert_summarizer')

f = open("0seq2seqres.csv", "w")

filepath = '/kaggle/input/headlinerdata/test.txt'
with open(filepath) as fp:
   line = fp.readline()
   while line:
    #    print("Line {}: {}".format(cnt, line.strip()))
       prd = best_summarizer.predict(line.strip())
       prd.strip(' [SEP]')
       outline = line.strip() + ' --> ' + prd +'\n'
    #    outline = outline.strip('[SEP]')
       f.write(outline) 
       line = fp.readline()

fp.close()       
f.close()




