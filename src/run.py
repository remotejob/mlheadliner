# from headliner.trainer import Trainer
# from headliner.model.transformer_summarizer import TransformerSummarizer

# data = [('You are the stars, earth and sky for me!', 'I love you.'),
#         ('You are great, but I have other plans.', 'I like you.')]

# summarizer = TransformerSummarizer(embedding_size=64, max_prediction_len=20)
# trainer = Trainer(batch_size=2, steps_per_epoch=100)
# trainer.train(summarizer, data, num_epochs=2)
# summarizer.save('/tmp/summarizer')

# summarizer = TransformerSummarizer.load('/tmp/summarizer')
# print(summarizer.predict('You are the stars, earth and sky for me!'))

import io

def create_dataset(path, num_examples):
    lines = io.open(path, encoding='UTF-8').read().strip().split('\n')
    word_pairs = [[w for w in l.split('\t')[:2]]  for l in lines[:num_examples]]
    return zip(*word_pairs)

# eng, ger, meta = create_dataset('deu.txt', 30000)
# eng, ger = create_dataset('askans.txt', 30000)
eng, ger = create_dataset('askans.txt',927197)
data = list(zip(eng, ger))

from sklearn.model_selection import train_test_split

train, test = train_test_split(data, test_size=200)

from headliner.preprocessing import Preprocessor

preprocessor = Preprocessor(lower_case=True)
train_prep = [preprocessor(t) for t in train]

from tensorflow_datasets.core.features.text import SubwordTextEncoder
from headliner.preprocessing import Vectorizer

inputs_prep = [t[0] for t in train_prep]
targets_prep = [t[1] for t in train_prep]
tokenizer_input = SubwordTextEncoder.build_from_corpus(
    inputs_prep, target_vocab_size=2**13,
    reserved_tokens=[preprocessor.start_token, preprocessor.end_token])
tokenizer_target = SubwordTextEncoder.build_from_corpus(
    targets_prep, target_vocab_size=2**13, 
    reserved_tokens=[preprocessor.start_token, preprocessor.end_token])

vectorizer = Vectorizer(tokenizer_input, tokenizer_target)
'vocab size input {}, target {}'.format(
    vectorizer.encoding_dim, vectorizer.decoding_dim)


from headliner.model.transformer_summarizer import TransformerSummarizer
from headliner.trainer import Trainer

summarizer = TransformerSummarizer(num_heads=4,
                                   feed_forward_dim=1024,
                                   num_layers=1,
                                   embedding_size=256,
                                   dropout_rate=0.1,
                                   max_prediction_len=100)
summarizer.init_model(preprocessor, vectorizer)
trainer = Trainer(steps_per_epoch=250,
                  batch_size=64, #32
                  model_save_path='/tmp/transformer_summarizer',
                  tensorboard_dir='/tmp/transformer_tensorboard',
                  steps_to_log=50)
trainer.train(summarizer, train, num_epochs=1000, val_data=test)


best_summarizer = TransformerSummarizer.load('/tmp/transformer_summarizer')
# best_summarizer.predict('Do you like robots?')

tst =['kuka ihminen ei oot täydellinen','et vadtannut','putin','mikä kello aika sinnun sopii','minkalainen sinulla on','do you speak english','sovitaan treffit','oikean kokoinen se on']


print('-----------------------------')
for ts in tst:
    prd = best_summarizer.predict(ts)
    print(ts,'-->',prd)